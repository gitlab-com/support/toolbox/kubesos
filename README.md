# KubeSOS

`kubectl` and `helm` wrapper to retrieve GitLab cluster configuration and logs from GitLab Cloud Native chart deployments

# Not using kubernetes?

We also have [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos), which serves the same purpose for omnibus based gitlab installations.

# What's the relationship between kubeSOS and GitLabSOS?

Both are used to make debugging easier, and attempt to get relevant information from environments. They are not dependencies of each other, just sibling projects!

# I want to add something new to KubeSOS!

You can! Also consider adding it to [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos) as well if it's relevant!

#### Requirements
[kubectl client v1.14+](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[helm 2.12+](https://helm.sh/docs/intro/quickstart/) 

#### Usage

| Flags | Description | Required   | Default
| :---- | :---------- | :--------- | :------
| `-n`  | namespace   | No | "default"
| `-N`  | use pod names instead of Kubernetes app annotations (see WARNING below) | No |
| `-r`  | helm chart release | No | "gitlab"
| `-l app`  | application label to match for logs (can be used multiple times) | No | 
| `-L` | select apps for logs interactively | No | n/a
| `-s time`  | Only return logs newer than a relative duration like 5s, 2m, or 3h | No | 0=all logs
| `-t time_stamp`  | Only return logs after a specific date (RFC3339) | No | all logs
| `-m maxlines` | Override the default maximum lines output per log (-1 = no limit) | No | 10000
| `-p` | Prepend log entries with pod and container names | No | n/a
| `-w log_timeout` | Log generation wait time (seconds). Increase this if log collection does not complete in time  | No | 60

> WARNING: Please exercise caution and due diligence in sanitizing the captured data/logs prior to submitting kubeSOS output to your support ticket. Special attention should be paid to enabling the `-N` option which will allow the potential collection of logs from non-GitLab related pods. 

Download and execute:

```
chmod +x kubeSOS.sh
./kubeSOS.sh [flags]
```

Or use `curl`:

```
curl https://gitlab.com/gitlab-com/support/toolbox/kubesos/raw/main/kubeSOS.sh | bash -s -- [flags]
```

Data will be archived to `kubesos-<timestamp>.tar.gz`

### Examples

```bash
# Capture logs for any app containing 'web' or 'git' from the previous 6 hours
./kubeSOS.sh -l web -l git -s 6h

# Capture all logs since midnight 27th June 2023
./kubeSOS.sh -t 2023-06-27T00:00:00Z

# Capture last 10000 lines of all logs against specific release/namespace
# Interactively choose logs to capture
./kubeSOS.sh -n gitlab-deploy -r gitlab-aws -L
